<?php

namespace Dls\Entity\V0;

use Illuminate\Support\Collection;

class Response
{
    protected $isSuccess;

    protected $message = '';

    protected $jump = '';

    protected $data = [];

    public function __construct(bool $isSuccess)
    {
        $this->isSuccess = $isSuccess;
    }

    public static function success()
    {
        return new static(true);
    }

    public static function error()
    {
        return new static(false);
    }

    public function message(string $data)
    {
        $this->message = $data;
        return $this;
    }

    public function jump(string $data)
    {
        $this->jump = $data;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     *
     */
    public function with($data)
    {
        if (is_string($data)) {
            $this->data = $data;
            return $this;
        }

        if (is_array($data)) {
            $this->data = $data;
            return $this;
        }

        if (is_object($data)) {
            if ($data instanceof Collection) {
                $this->data = $data->toArray();
                return $this;
            }
        }

        throw new \Exception('Server Api Response Param Unexpected.');
    }

    /**
     * 通过JSONP返回数据
     *
     * @param array $append
     * @param string $callbackKey
     *
     */
    public function jsonp($append = [], $callbackKey = 'jsoncallback')
    {
        $callback = request()->get($callbackKey) ?: 'callback';

        $ret = [
            'status' => $this->isSuccess,
            'msg' => $this->message,
            'jump' => $this->jump,
            'data' => $this->data,
        ];
         if (! empty($append)) {
             $ret = array_merge($ret, $append);
         }

        return response()->jsonp($callback, $ret);
    }


    /**
     * 直接返回json数据
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function json()
    {
        return response()->json([
            'status' => $this->isSuccess,
            'msg' => $this->message,
            'jump' => $this->jump,
            'data' => $this->data,
        ]);
    }
}