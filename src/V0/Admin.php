<?php

namespace Dls\Entity\V0;

use Dls\Entity\V0\Table;
use Dls\Entity\V0\Layout\Content;
use Closure;
use Illuminate\Support\Facades\Config;

/**
 * Class Admin.
 */
class Admin
{

    /**
     * @param $model
     * @param Closure $callable
     *
     */
    public function table(Closure $callable)
    {
        return new Table($callable);
    }

    /**
     * @param $model
     * @param Closure $callable
     *
     */
    public function grid(Closure $callable)
    {
        return new Grid($callable);
    }

    /**
     * @param $model
     * @param Closure $callable
     * @return Form
     */
    public function form($model, Closure $callable)
    {
        return new Form($model, $callable);
    }


    /**
     * @param Closure|null $callable
     * @return Content
     */
    public function content(Closure $callable = null)
    {
        return new Content($callable);
    }

    /**
     * Get admin title.
     *
     * @return Config
     */
    public function title()
    {
        return config('entity.title');
    }

}
