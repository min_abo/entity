<?php

namespace Dls\Entity\V0\Traits;


use Dls\Entity\V0\Form;
use Dls\Entity\V0\Grid;
use Dls\Entity\V0\Table;
use Dls\Entity\V0\Layout\Content;

trait AdminBuilder
{
    /**
     * @param \Closure $callback
     *
     * @return Table
     */
    public static function table(\Closure $callback)
    {
        return new Table(new static(), $callback);
    }

    /**
     * @param \Closure $callback
     *
     * @return Form
     */
    public static function form(\Closure $callback)
    {
        Form::registerBuiltinFields();

        return new Form(new static(), $callback);
    }

    public static function content(\Closure $callable)
    {
        return new Content($callable);
    }

    /**
     * @param \Closure $callback
     *
     * @return Grid
     */
    public static function grid(\Closure $callback)
    {
        return new Grid(new static(), $callback);
    }
}
