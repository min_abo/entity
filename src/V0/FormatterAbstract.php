<?php

namespace Dls\Entity\V0;

use Dls\Entity\V0\Grid\Column;

abstract class FormatterAbstract
{
    abstract public function formatter(Column $field): string;

}