define(function (require, exports, module) {
    require('jquery');

    var notice = require('core/tips/notice');
    var Export = function (obj) {
        this.obj = obj;
    };

    Export.prototype.config = {};

    Export.prototype.getRelay = function (url) {
        //把url中所有<{}>找出来
        if (!url) {
            return [];
        }
        url = url.match(/<\{([^}]+)\}>/ig);
        for (var i in url) {
            url[i] = url[i].substr(2, url[i].length - 4);
        }
        return url ? url : [];
    };


    Export.prototype.initAction = function () {
        var _this = this;
        $('body').on('click', '.exportCsv', function (event) {
            event.preventDefault();
            _this.config.url = $(this).attr('href');

            var noticeId = notice.on('开始下载...', 'info');
            _this.load(function () {
                notice.off(noticeId);
                notice.on('下载成功', 'success');
            })
        });

    };

    Export.prototype.load = function (callback) {
        var relay = this.getRelay(this.config.url), _o, val;
        var aLink = document.createElement('a');
        var url = this.config.url;
        for (var i in relay) {
            _o = $('input[name="' + relay[i] + '"], select[name="' + relay[i] + '"]').first();
            val = _o.attr('type') === 'checkbox' ? (_o.prop("checked") ? _o.val() : '') : _o.val();
            url = url.replace('<{' + relay[i] + '}>', encodeURIComponent(val));
        }
        aLink.href = url;
        aLink.download = '';
        aLink.click();

        callback();
    };

    Export.prototype.init = function () {
        this.initAction();
    };

    module.exports = Export;

});