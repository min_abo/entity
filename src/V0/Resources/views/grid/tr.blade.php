<tr>
   @foreach($ths as $th)
        {!! $th->render() !!}
   @endforeach
   @if(!$actions->isEmpty())
           <th data-tableField="@php echo $actions->implode(' ');@endphp"></th>
   @endif
</tr>