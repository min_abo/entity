<div class="p-md">
    <div class="row">
        {!! $grid->renderHeaderTools() !!}
    </div>
    <span>&nbsp;</span>
    <div class="row">
        <table class="{{ $grid->getClass() }}"
               id="{{ $grid->getCallbackId() }}"
               data-fn="{{ $grid->getDataFn() }}"
               data-url="{{ $grid->getTableUrl() }}"
                {{ $grid->usePagination() ? "data-pageNum=" . $grid->getPageNum() : '' }}>
            <thead>
            @foreach($grid->columns() as $column)
                @if ($column->isHide())
                    @continue
                @endif
                <th data-class="text-nowrap" data-tableField="{!! strpos($column->getAlias(), '<{') ? $column->getAlias() : '<{' . $column->getAlias() . '}>' !!}"
                    @if(!empty($column->getReplace())) data-replace="{!! $column->getReplace()  !!}" @endif
                    @if(!empty($column->getHtmlClass())) class="{!! $column->getHtmlClass()  !!}" @endif
                >{!! $column->getLabel() !!}</th>
            @endforeach
            @foreach($grid->tools() as $tool)
                @if($tool instanceof \Illuminate\Contracts\Support\Renderable)
                    <th data-class="text-nowrap" data-tableField="{{ $tool->render() }}">{{ $tool->title() }}</th>
                @else
                    <th data-class="text-nowrap" data-tableField="{{ $tool }}"></th>
                @endif
            @endforeach
            </thead>
        </table>
    </div>
</div>
