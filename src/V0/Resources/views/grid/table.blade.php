@extends('entity::layout.partial')
@section('content')
<div class="panel-body">
    <div class="p-md">
        <table
                class="{{ $table->getClass() }}"
               data-fn="data-table layout-window-open"
               data-url="{{ $table->getTableUrl() }}"
               @if($table->usePagination()) {{ "data-pageNum=" . $table->getPageNum() }} @endif>
            <thead>
                @foreach($table->columns() as $column)
                    <th data-tableField="{!! '<{' . $column->getName() . '}>' !!}"
                        @if($column->getReplace()) data-replace={!! $column->getReplace() !!} @endif >
                        {{ $column->getLabel() }}
                    </th>
                @endforeach
            </thead>
        </table>
    </div>
</div>
@endsection