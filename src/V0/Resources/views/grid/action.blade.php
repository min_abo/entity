<th data-tableField="@php echo '<{'. $field .'}>' @endphp"
    data-frozen="{{ $frozen }}"
    data-filter="{{ $filter }}"
    data-highlight="{{ $highlight }}"
    data-class="{{ $class }}"
    data-sum="{{ $sum }}"
    data-replace="{{ $replace }}">
    {{ $text }}
</th>