<!DOCTYPE html>
<html lang="zh">

@include('entity::layout.header')

<body>
<div class="app app-header-fixed">
    <div class="p-md">

        {!! $form->open(['class' => "form-horizontal", 'role' => 'form', 'data-fn' => 'form-ajax']) !!}
        @if(!$form->getPanel()->isEmpty())
            @include('entity::form.panel', ['panel' => $form->getPanel()])
        @else
            @foreach($form->fields() as $field)
                {!! $field->render() !!}
            @endforeach
        @endif

        @if( ! $form->isMode(\Dls\Entity\V0\Form\Builder::MODE_VIEW)  || ! $form->option('enableSubmit'))
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @endif
        <div class="col-md-{{$width['label']}}">

        </div>
        <div class="col-md-{{$width['field']}}">

            {!! $form->submitButton() !!}

            {!! $form->resetButton() !!}

        </div>

        @foreach($form->getHiddenFields() as $hiddenField)
            {!! $hiddenField->render() !!}
        @endforeach

        {!! $form->close() !!}
    </div>
    <footer id="footer" class="app-footer" role="footer">
        <a href="javascript:;" data-fn="view-image"></a>
    </footer>
</div>

<script src="{{ env('WUI_HOST')}}/core.js" data-deps="bootstrap"
        data-config="{{ env('WUI_HOST')}}/config.js" data-main="init"></script>
</body>
</html>
