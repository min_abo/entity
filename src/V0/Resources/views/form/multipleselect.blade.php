<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

    <label for="{{$id}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}}">

        @include('entity::form.error')

        <input type="hidden" data-fn="form-select" name="{{$name}}" data-multi=true
               {{ $groupBy ? 'data-groupBy=' . $groupBy : '' }} value="{{ $value }}"
               data-enum="{{ $enum }}" id="{{ $id }}"
                {{ $dataKey ? 'data-value='.$dataKey : ''}}
                {{ $dataValue ? 'data-key='.$dataValue : ''}} />

        @include('entity::form.help-block')

    </div>
</div>
