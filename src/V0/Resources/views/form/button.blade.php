<div class="{{$viewClass['form-group']}}">

    <label class="{{$viewClass['label']}} control-label"></label>

    <div class="{{$viewClass['field']}}">
        <input type='{{ $type }}' value='{{$label}}' class="btn {{ $class }}" {!! $attributes !!} />
    </div>
</div>