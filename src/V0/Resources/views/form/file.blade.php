<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

    <label for="{{$id}}" class="{{$viewClass['label']}} control-label text-right">{{$label}}</label>

    <div class="{{$viewClass['field']}}">

        @include('entity::form.error')

        <input type="hidden" name="{{$name}}" data-fn="{{ !empty($image) ? 'form-file-image' : 'form-file' }}"
               @if(!empty($filters)) {{ "data-filters=$filters" }} @endif
               @if(!empty($maxSize)) {{ "data-max_file_size=$maxSize" }} @endif
               @if(!empty($type)) {{ "data-type=$type" }} @endif
               @if(isset($multi) && !$multi) {{ "data-single=true" }} @endif
                value="{{ $value }}" {!! $attributes !!}/>

        @include('entity::form.help-block')

    </div>
</div>
