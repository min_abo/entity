<div class="{{$viewClass['form-group']}}">
    <label class="{{$viewClass['label']}} text-right">{{$label}}</label>
    <div class="{{$viewClass['field']}}">
        <span class="form-control-static">
            {!! $value !!}&nbsp;
        </span>

        @include('entity::form.help-block')

    </div>
</div>