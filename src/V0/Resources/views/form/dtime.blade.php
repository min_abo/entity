<div class="{{$viewClass['form-group']}}">

    <label class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}}">
        @include('entity::form.error')

        <div class="row" style="width: 390px">
            <div class="col-lg-6">
                <input type="hidden" data-fn="form-datepicker"
                       data-timePicker="true" data-locale-format="YYYY-MM-DD HH:mm:ss"
                       data-timePicker24Hour="true" data-timePickerSeconds="true"
                       data-autoApply="false"
                       name="{{$name}}"
                       value="{{$value}}"
                       style="width: 160px"
                />
            </div>
        </div>

        @include('entity::form.help-block')

    </div>
</div>
