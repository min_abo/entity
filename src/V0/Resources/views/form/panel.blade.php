
@foreach($panel->getPanels() as $panels)
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ $panels['title'] }}
        </div>
        <div class="panel-body">
            @foreach($panels['fields'] as $field)
                @if($field instanceof \App\Backend\Form\Field\Custom)
                    @foreach($field->render() as $item)
                        {!! $item->render() !!}
                    @endforeach
                @else
                    {!! $field->render() !!}
                @endif
            @endforeach
        </div>
    </div>
@endforeach

