<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

<label for="{{$id}}" class="{{$viewClass['label']}} control-label text-right">{{$label}}</label>

    <div class="{{$viewClass['field']}}">

        @include('entity::form.error')

        <input type="hidden"
                name="{{$name}}"
                value="{{ $value }}"
                @if(! empty($groupBy)) data-groupBy="{{ $groupBy }}"  @endif
                @if(! empty($multi)) data-multi="{{ 'true' }}"  @endif
                @if(! empty($enum)) data-enum="{{ $enum }}"  @endif
                @if(! empty($dataKey)) data-key="{{ $dataKey }}"  @endif
                @if(! empty($dataValue)) data-value="{{ $dataValue }}"  @endif
                @if(! empty($size)) data-size="{{ $size }}"  @endif
                {!! $attributes !!}
        />
    </div>

        @include('entity::form.help-block')

</div>
