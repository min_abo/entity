<header id="header" class="app-header bg-dark navbar" role="menu">
    <!-- navbar header -->
    <div class="navbar-header dk">
        <a class="navbar-brand text-lt">
            <i class="pull-right ti-arrow-circle-left text-sm m-v-xs m-l-xs"></i>
            <i class="glyphicon glyphicon-th-large text-md"></i>
            <span class="hidden-folded m-l-xs">{{ config('entity.title') }}</span>
        </a>
    </div>
    <!-- / navbar header -->

    <!-- navbar collapse -->
    <div class="navbar-collapse box-shadow-md hidden-xs">
        <ul class="nav navbar-nav navbar-left">
        </ul>

        <ul class="nav navbar-nav navbar-right m-r-n">
            <li class="dropdown m-r">
                <a href="#" class="clear no-padding-h" data-toggle="dropdown">
                    <span class="hidden-sm m-l"><span
                                class="glyphicon glyphicon-user"></span> {{ $principal_name ?? '' }}</span>
                    <b class="caret m-h-xs hidden-sm"></b>
                </a>
                <ul class="dropdown-menu pull-right no-b-t">
                    <li>
                        <a href="{{ $user_detail ?? '' }}">个人信息</a>
                    </li>
                    <li class="divider">
                    <li>
                        <a href="{{ $logout ?? ''}}">退出</a>
                    </li>
                </ul>
            </li>
            <li class="m-r">
                <a href="" id="docHelper" class="text-white">
                </a>
            </li>
        </ul>
    </div>
    <!-- / navbar collapse -->
</header>


