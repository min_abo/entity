<footer id="footer" class="app-footer" role="footer">
    <div class="p bg-white text-xs">
        <div class="pull-right hidden-xs hidden-sm text-muted">
            <strong>{{ request()->getHost() }}</strong>&copy; Copyright 2018
        </div>
        <ul class="list-inline no-margin text-center-xs">
            <a href="#" data-fn="layout-window-open"></a>
            <li class="text-muted">&nbsp;</li>
        </ul>
    </div>
</footer>
