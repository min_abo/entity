<!DOCTYPE html>
<html lang="zh">

@include('entity::layout.header')

<body data-fn="app-doc">
<div class="app app-header-fixed">
    @include('entity::layout.nav')
    @include("entity::layout.menu")
    <div id="content" class="app-content" role="main">
        @yield('content')
    </div>

    @include('entity::layout.footer')
</div>

<script src="{{ env('WUI_HOST')}}/core.js" data-deps="bootstrap"
        data-config="{{ env('WUI_HOST')}}/config.js" data-main="init"></script>
</body>
</html>