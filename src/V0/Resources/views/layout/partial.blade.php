<!DOCTYPE html>
<html lang="zh">

@include('entity::layout.header')

<body>
<div class="app app-header-fixed">
    <div id="content" class="app-content" role="main">
        @yield('content')
    </div>
    @include('entity::layout.footer')
</div>
<script src="{{ env('WUI_HOST')}}/core.js" data-deps="bootstrap"
        data-config="{{ env('WUI_HOST')}}/config.js" data-main="init"></script>
</body>
</html>