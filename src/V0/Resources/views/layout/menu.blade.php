<aside id="aside" class="app-aside hidden-xs bg-light lt text-xs">
    <div class="app-aside-inner">
        <div class="app-aside-body scrollable hover">
            <nav data-fn="layout-aside">
            </nav>
        </div>
        <div class="app-aside-footer p-v-sm text-center">
            <span id="asideTime"></span>
        </div>
    </div>
</aside>