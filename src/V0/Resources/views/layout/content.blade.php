@extends('entity::layout.main')

@section('content')
    <div class="p-md">
        <div class="p-h-md p-v-xs bg-white box-shadow pos-rlt text-muted">
            <h3 class="no-margin m-v-xs l-h text-lg">
                <span class="{{ $header['icon'] }}">{{ $header['title'] }}</span>
                <span class="block text-xs m-t-xs">
                    @if(!empty($searcher))
                        @foreach($searcher as $view)
                            <label class="m-r">
                                {!! $view->render() !!}
                            </label>
                        @endforeach
                        <label data-fn="data-form-holdButton">
                            <div class="input-group m-r">
                                <button type="button" data-action="continue" class="btn btn-sm btn-info">
                                <span class="ti-search"></span> 查询</button>
                            </div>
                        </label>
                    @endif
                </span>
            </h3>
        </div>
        <div class="panel panel-default">
            {!! $content !!}
        </div>
    </div>
@endsection
