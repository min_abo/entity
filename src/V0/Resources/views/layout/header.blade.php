<head>
    <title> {{ config('entity.title') }} </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!-- 引入wui -->
    <link href="{{ config('entity.wuihost') }}/lib/heymetro/css/app.min.css" rel="stylesheet">

</head>