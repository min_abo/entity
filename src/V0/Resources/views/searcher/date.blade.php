<div class="input-group">
    <label for="date">日期：</label>
    <input type="hidden" data-type="sm"
           data-fn="form-datepicker-multi"
           name="date_range"
           @if(!empty($name)) {{ "name=$name" }} @else {{ "name=date_range" }} @endif
           @if(!empty($extLink)) {{ "data-extLink=true" }} @endif
           @if(!empty($maxDate)) {{ "data-maxDate=$maxDate" }} @endif
           @if(!empty($value)) {{ "value=$value" }} @endif
           @if(!empty($minDate)) {{ "data-minDate=$minDate" }} @endif
    />
</div>