<div class="input-group">
    <input type="text"
           class="form-control input-sm"
           name="{{ $name }}"
           @if(!empty($text)) {{ "placeholder=$text" }} @endif
    >
</div>