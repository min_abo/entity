<div class="input-group">
    <label for="id">主键：</label>
    <input type="hidden" data-fn="form-select" name="id" data-multi="true"
           data-enum="{{ route('user.id.list') }}"
           data-size="text"
           data-value="value"
           data-key="key" value="{{ request('id', '')}}"
    >
</div>