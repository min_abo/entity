<div class="input-group">
    <label for="{{ $name }}">{{ $text }}：</label>
    <input type="hidden"
           data-fn="form-select"
           name="{{ $name }}"
           @if(!empty($multi)) {{ "data-multi=true" }} @endif
           data-enum="{{ $enum }}"
           data-size="sm"
           @if(!empty($value)) {{ "data-value=$value" }} @endif
           @if(!empty($key)) {{ "data-key=$key" }} @endif
           value="{{ request($name, '')}}"
    >
</div>