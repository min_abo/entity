<?php

namespace Dls\Entity\V0\Searchable;

use Dls\Entity\V0\Grid\Column;
use Dls\Entity\V0\Searcher;
use Illuminate\Database\Eloquent\Builder;

class LikeSearcher extends Searcher
{

    public function search(Builder $builder)
    {
        $searchKey = $this->column->getName();
        $value = request()->input($searchKey);

        if (!$value || $value === 'undefined' || $value === 0 || $value === '请选择') {
            return $builder;
        }

        return $builder->where($searchKey, 'like', "%{$value}%");
    }

}