<?php

namespace Dls\Entity\V0\Searchable;


use Dls\Entity\V0\Searcher;
use Illuminate\Database\Eloquent\Builder;

class ListSearcher extends Searcher
{
    public function search(Builder $builder)
    {
        $searchKey = $this->column->getName();
        $value = request()->input($searchKey);

        if (blank($value) || $value == 'undefined') {
            return $builder;
        }

        $value = explode(',', $value);

        if (!is_array($value)) {
            $value = [(int)$value];
        }
        $list = [];
        foreach ($value as $item) {
            $list[] = $item == -1 ? '' : $item;
        }

        return $builder->whereIn($searchKey, $list);
    }

}