<?php

namespace Dls\Entity\V0\Searchable;

use Dls\Entity\V0\Grid\Column;
use Dls\Entity\V0\Searcher;
use Illuminate\Database\Eloquent\Builder;

class DateSearcher extends Searcher
{
    protected $shouldEqual;

    public function __construct(Column $column, $shouldEqual = null)
    {
        $this->column = $column;
        $this->shouldEqual = $shouldEqual;
    }

    public function search(Builder $builder)
    {
        $searchKey = $this->column->getAlias() ?? $this->column->getName();
        $inputName = 'date';

        $input = request()->get($inputName);
        if (blank($input) || $input == 'undefined') {
            return $builder;
        }

        return $builder->where($searchKey, '=', $start);
    }

}