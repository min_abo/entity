<?php

namespace Dls\Entity\V0\Searchable;

use Dls\Entity\V0\Grid\Column;
use Dls\Entity\V0\Searcher;
use Illuminate\Database\Eloquent\Builder;

class DateRangeSearcher extends Searcher
{

    public function search(Builder $builder)
    {
        $searchKey = $this->column->getAlias() ?? $this->column->getName();
        $inputName = 'date_range';

        $input = request()->get($inputName);
        if (blank($input) || $input == 'undefined') {
            return $builder;
        }

        list($start, $end) = explode('~', $input);
        $start = str_replace('-', '', $start);
        $end = str_replace('-', '', $end);

        return $builder->where($searchKey, '>=', $start)
            ->where($searchKey, '<=', $end);
    }

}