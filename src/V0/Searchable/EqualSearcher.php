<?php

namespace Dls\Entity\V0\Searchable;

use Dls\Entity\V0\Searcher;
use Illuminate\Database\Eloquent\Builder;

class EqualSearcher extends Searcher
{
    protected $shouldEqual;

    public function __construct($shouldEqual = null)
    {
        $this->shouldEqual = $shouldEqual;
    }

    public function search(Builder $builder)
    {
        $searchKey = $this->column->getName();

        if (is_null($this->shouldEqual)) {
            $value = request()->input($searchKey);
            if (blank($value) || $value == 'undefined') {
                return $builder;
            }
            return $builder->where($searchKey, $value);

        } else {
            return $builder->where($searchKey, $this->shouldEqual);
        }
    }

}