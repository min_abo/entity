<?php

return [

    'controller' => app_path('Http') . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR . 'Entity',

    'namespace' => 'App\\Entity\\',
    
    'wuihost' =>  env('WUI_HOST', 'http://wui.local.superdalan.com'),

    'title' => '后台'
];