<?php

namespace Dls\Entity\V0;

use Dls\Entity\V0\Form\Builder;
use Dls\Entity\V0\Form\Field;
use Dls\Entity\V0\Form\Field\Button;
use Dls\Entity\V0\Form\Field\Captcha;
use Dls\Entity\V0\Form\Field\Checkbox;
use Dls\Entity\V0\Form\Field\Color;
use Dls\Entity\V0\Form\Field\Currency;
use Dls\Entity\V0\Form\Field\Custom;
use Dls\Entity\V0\Form\Field\Date;
use Dls\Entity\V0\Form\Field\DateRange;
use Dls\Entity\V0\Form\Field\Datetime;
use Dls\Entity\V0\Form\Field\DTime;
use Dls\Entity\V0\Form\Field\DatetimeRange;
use Dls\Entity\V0\Form\Field\Decimal;
use Dls\Entity\V0\Form\Field\Display;
use Dls\Entity\V0\Form\Field\Divide;
use Dls\Entity\V0\Form\Field\Editor;
use Dls\Entity\V0\Form\Field\Email;
use Dls\Entity\V0\Form\Field\Embeds;
use Dls\Entity\V0\Form\Field\File;
use Dls\Entity\V0\Form\Field\Hidden;
use Dls\Entity\V0\Form\Field\Html;
use Dls\Entity\V0\Form\Field\Icon;
use Dls\Entity\V0\Form\Field\Id;
use Dls\Entity\V0\Form\Field\Image;
use Dls\Entity\V0\Form\Field\Ip;
use Dls\Entity\V0\Form\Field\Listbox;
use Dls\Entity\V0\Form\Field\Map;
use Dls\Entity\V0\Form\Field\Mobile;
use Dls\Entity\V0\Form\Field\Month;
use Dls\Entity\V0\Form\Field\MultipleFile;
use Dls\Entity\V0\Form\Field\MultipleImage;
use Dls\Entity\V0\Form\Field\MultipleSelect;
use Dls\Entity\V0\Form\Field\Number;
use Dls\Entity\V0\Form\Field\Password;
use Dls\Entity\V0\Form\Field\Radio;
use Dls\Entity\V0\Form\Field\Rate;
use Dls\Entity\V0\Form\Field\Select;
use Dls\Entity\V0\Form\Field\Slider;
use Dls\Entity\V0\Form\Field\SwitchField;
use Dls\Entity\V0\Form\Field\Tags;
use Dls\Entity\V0\Form\Field\Text;
use Dls\Entity\V0\Form\Field\Textarea;
use Dls\Entity\V0\Form\Field\Time;
use Dls\Entity\V0\Form\Field\TimeRange;
use Dls\Entity\V0\Form\Field\Url;
use Dls\Entity\V0\Form\Field\Year;
use Dls\Entity\V0\Form\Panel;
use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;

/**
 * Class Form.
 *
 * @method Text           text($column, $label = '')
 * @method Checkbox       checkbox($column, $label = '')
 * @method Radio          radio($column, $label = '')
 * @method Select         select($column, $label = '')
 * @method MultipleSelect multipleSelect($column, $label = '')
 * @method Textarea       textarea($column, $label = '')
 * @method Hidden         hidden($column, $label = '')
 * @method Id             id($column, $label = '')
 * @method Ip             ip($column, $label = '')
 * @method Url            url($column, $label = '')
 * @method Color          color($column, $label = '')
 * @method Email          email($column, $label = '')
 * @method Mobile         mobile($column, $label = '')
 * @method Slider         slider($column, $label = '')
 * @method Map            map($latitude, $longitude, $label = '')
 * @method Editor         editor($column, $label = '')
 * @method File           file($column, $label = '')
 * @method Image          image($column, $label = '')
 * @method Date           date($column, $label = '')
 * @method Datetime       datetime($column, $label = '')
 * @method DTime          dtime($column, $label = '')
 * @method Time           time($column, $label = '')
 * @method Year           year($column, $label = '')
 * @method Month          month($column, $label = '')
 * @method DateRange      dateRange($start, $end, $label = '')
 * @method DateTimeRange  datetimeRange($start, $end, $label = '')
 * @method TimeRange      timeRange($start, $end, $label = '')
 * @method Number         number($column, $label = '')
 * @method Currency       currency($column, $label = '')
 * @method HasMany        hasMany($relationName, $callback)
 * @method SwitchField    switch($column, $label = '')
 * @method Display        display($column, $label = '')
 * @method Rate           rate($column, $label = '')
 * @method Divide         divider()
 * @method Password       password($column, $label = '')
 * @method Decimal        decimal($column, $label = '')
 * @method Html           html($html, $label = '')
 * @method Tags           tags($column, $label = '')
 * @method Icon           icon($column, $label = '')
 * @method Embeds         embeds($column, $label = '')
 * @method MultipleImage  multipleImage($column, $label = '')
 * @method MultipleFile   multipleFile($column, $label = '')
 * @method Captcha        captcha($column, $label = '')
 * @method Listbox        listbox($column, $label = '')
 * @method Button         button($column, $label = '')
 * @method Custom         custom($column, $label = '')
 */
class Form
{
    /**
     * Eloquent model of the form.
     *
     * @var Model
     */
    protected $model;

    /**
     * @var \Illuminate\Validation\Validator
     */
    protected $validator;

    /**
     * @var Builder
     */
    protected $builder;


    /**
     * Saving callback.
     *
     * @var Closure
     */
    protected $saving;

    /**
     * Saved callback.
     *
     * @var Closure
     */
    protected $saved;

    /**
     * Data for save to current model from input.
     *
     * @var array
     */
    protected $updates = [];

    /**
     * Data for save to model's relations from input.
     *
     * @var array
     */
    protected $relations = [];

    /**
     * Input data.
     *
     * @var array
     */
    protected $inputs = [];

    /**
     * Available fields.
     *
     * @var array
     */
    public static $availableFields = [];

    /**
     * Ignored saving fields.
     *
     * @var array
     */
    protected $ignored = [];

    /**
     * Collected field assets.
     *
     * @var array
     */
    protected static $collectedAssets = [];

    /**
     * @var Panel
     */
    protected $panel = null;

    /**
     * Remove flag in `has many` form.
     */
    const REMOVE_FLAG_NAME = '_remove_';

    /**
     * Field rows in form.
     *
     * @var array
     */
    public $rows = [];

    /**
     * Create a new form instance.
     *
     * @param $model
     * @param \Closure $callback
     */
    public function __construct($model, Closure $callback)
    {
        $this->model = $model;

        $this->builder = new Builder($this);

        $callback($this);
    }

    /**
     * @param Field $field
     *
     * @return $this
     */
    public function pushField(Field $field)
    {
        $field->setForm($this);

        $this->builder->fields()->push($field);

        return $this;
    }

    /**
     * @return Model
     */
    public function model()
    {
        return $this->model;
    }

    /**
     * @return Builder
     */
    public function builder()
    {
        return $this->builder;
    }

    /**
     * Generate a edit form.
     *
     * @param $id
     *
     * @return $this
     */
    public function edit($id)
    {
        $this->builder->setMode(Builder::MODE_EDIT);
        $this->builder->setResourceId($id);

        $this->setFieldValue($id);

        return $this;
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function view($id)
    {
        $this->builder->setMode(Builder::MODE_VIEW);
        $this->builder->setResourceId($id);

        $this->setFieldValue($id);

        return $this;
    }

    /**
     * Use tab to split form.
     *
     * @param string  $title
     * @param Closure $content
     *
     * @return $this
     */
    public function panel($title, Closure $content)
    {
        $this->getPanel()->append($title, $content);

        return $this;
    }

    /**
     * Get Tab instance.
     *
     * @return Panel
     */
    public function getPanel()
    {
        if (is_null($this->panel)) {
            $this->panel = new Panel($this);
        }

        return $this->panel;
    }


    /**
     * Destroy data entity and remove files.
     *
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        $ids = explode(',', $id);

        foreach ($ids as $id) {
            if (empty($id)) {
                continue;
            }
            $this->model->find($id)->delete();
        }

        return true;
    }


    /**
     * Store a new record.
     *
     * @return bool|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|mixed|null
     * @throws \Throwable
     */
    public function store()
    {

        $data = Input::all();

        // Handle validation errors.
        if ($validationMessages = $this->validationMessages($data)) {
            return Response::error()->message($validationMessages)->json();
        }

        $this->prepare($data);

        // 兼容自定义的connection
        if ($this->model->getConnectionName()) {
            $conn = DB::connection($this->model->getConnectionName());
        } else {
            $conn = DB::connection();
        }

        $conn->transaction(function () {

            $inserts = $this->prepareInsert($this->updates);

            foreach ($inserts as $column => $value) {
                $this->model->setAttribute($column, $value);
            }
            // 保存前回调
            $this->callSaving($this->model);

            $this->model->save();

            $this->updateRelation($this->relations);
            // 保存后回调
            $this->complete($this->saved);
        });

        if ($response = $this->ajaxResponse(trans('admin.save_succeeded'))) {
            return $response;
        }

        return $this->redirectAfterStore();
    }

    /**
     * Get RedirectResponse after store.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectAfterStore()
    {

        $url = Input::get(Builder::PREVIOUS_URL_KEY) ?: $this->resource(0);

        return redirect($url);
    }

    /**
     * Get ajax response.
     *
     * @param string $message
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    protected function ajaxResponse($message)
    {
        $request = Request::capture();

        // ajax but not pjax
        if ($request->ajax() && !$request->pjax()) {
            return Response::success()->message($message)->json();
        }

        return false;
    }

    /**
     * Prepare input data for insert or update.
     *
     * @param array $data
     *
     * @return mixed
     */
    protected function prepare($data = [])
    {

        $this->inputs = $this->removeIgnoredFields($data);

        $this->relations = $this->getRelationInputs($this->inputs);

        $this->updates = array_except($this->inputs, array_keys($this->relations));
    }

    /**
     * Remove ignored fields from input.
     *
     * @param array $input
     *
     * @return array
     */
    protected function removeIgnoredFields($input)
    {
        array_forget($input, $this->ignored);

        return $input;
    }

    /**
     * Get inputs for relations.
     *
     * @param array $inputs
     *
     * @return array
     */
    protected function getRelationInputs($inputs = [])
    {
        $relations = [];

        foreach ($inputs as $column => $value) {
            if (method_exists($this->model, $column)) {
                $relation = call_user_func([$this->model, $column]);

                if ($relation instanceof Relation) {
                    $relations[$column] = $value;
                }
            }
        }

        return $relations;
    }

    /**
     * Call saving callback.
     *
     * @return mixed
     */
    protected function callSaving(Model $model)
    {
        if ($this->saving instanceof Closure) {
            return call_user_func($this->saving, $model);
        }
    }

    /**
     * Callback after saving a Model.
     *
     * @param Closure|null $callback
     *
     * @return mixed|null
     */
    protected function complete(Closure $callback = null)
    {
        if ($callback instanceof Closure) {
            return $callback($this);
        }
    }

    /**
     * Handle update.
     *
     * @param $id
     * @param null $data
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|mixed|null|Response
     * @throws \Throwable
     */
    public function update($id, $data = null)
    {
        $data = ($data) ?: Input::all();

        $data = $this->handleEditable($data);

        /* @var Model $this->model */
        $this->model = $this->model->with($this->getRelations())->findOrFail($id);

        $this->setFieldOriginalValue();

        // Handle validation errors.
        if ($error = $this->validationMessages($data)) {

            return Response::error()->message($error)->json();
        }

        $this->prepare($data);
        // 兼容自定义的connection
        if ($this->model->getConnectionName()) {
            $conn = DB::connection($this->model->getConnectionName());
        } else {
            $conn = DB::connection();
        }

        $conn->transaction(function () {
            $updates = $this->prepareUpdate($this->updates);

            foreach ($updates as $column => $value) {
                /* @var Model $this->model */
                $this->model->setAttribute($column, $value);
            }

            $this->callSaving($this->model);

            $this->model->save();

            $this->updateRelation($this->relations);

            $this->complete($this->saved);
        });

        if ($response = $this->ajaxResponse(trans('admin.update_succeeded'))) {
            return $response;
        }

        return $this->redirectAfterUpdate();
    }

    /**
     * Get RedirectResponse after update.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectAfterUpdate()
    {

        $url = Input::get(Builder::PREVIOUS_URL_KEY) ?: $this->resource(-1);

        return redirect($url);
    }

    /**
     * Check if request is from editable.
     *
     * @param array $input
     *
     * @return bool
     */
    protected function isEditable(array $input = [])
    {
        return array_key_exists('_editable', $input);
    }

    /**
     * Handle editable update.
     *
     * @param array $input
     *
     * @return array
     */
    protected function handleEditable(array $input = [])
    {
        if (array_key_exists('_editable', $input)) {
            $name = $input['name'];
            $value = $input['value'];

            array_forget($input, ['pk', 'value', 'name']);
            array_set($input, $name, $value);
        }

        return $input;
    }

    /**
     * @param array $input
     *
     * @return array
     */
    protected function handleFileDelete(array $input = [])
    {
        if (array_key_exists(Field::FILE_DELETE_FLAG, $input)) {
            $input[Field::FILE_DELETE_FLAG] = $input['key'];
            unset($input['key']);
        }

        Input::replace($input);

        return $input;
    }


    /**
     * Update relation data.
     *
     * @param array $relationsData
     *
     * @return void
     */
    protected function updateRelation($relationsData)
    {
        foreach ($relationsData as $name => $values) {
            if (!method_exists($this->model, $name)) {
                continue;
            }

            $relation = $this->model->$name();

            $oneToOneRelation = $relation instanceof \Illuminate\Database\Eloquent\Relations\HasOne
                || $relation instanceof \Illuminate\Database\Eloquent\Relations\MorphOne;

            $prepared = $this->prepareUpdate([$name => $values], $oneToOneRelation);

            if (empty($prepared)) {
                continue;
            }

            switch (get_class($relation)) {
                case \Illuminate\Database\Eloquent\Relations\BelongsToMany::class:
                case \Illuminate\Database\Eloquent\Relations\MorphToMany::class:
                    if (isset($prepared[$name])) {
                        $relation->sync($prepared[$name]);
                    }
                    break;
                case \Illuminate\Database\Eloquent\Relations\HasOne::class:

                    $related = $this->model->$name;

                    // if related is empty
                    if (is_null($related)) {
                        $related = $relation->getRelated();
                        $related->{$relation->getForeignKeyName()} = $this->model->{$this->model->getKeyName()};
                    }

                    foreach ($prepared[$name] as $column => $value) {
                        $related->setAttribute($column, $value);
                    }

                    $related->save();
                    break;
                case \Illuminate\Database\Eloquent\Relations\MorphOne::class:
                    $related = $this->model->$name;
                    if (is_null($related)) {
                        $related = $relation->make();
                    }
                    foreach ($prepared[$name] as $column => $value) {
                        $related->setAttribute($column, $value);
                    }
                    $related->save();
                    break;
                case \Illuminate\Database\Eloquent\Relations\HasMany::class:
                case \Illuminate\Database\Eloquent\Relations\MorphMany::class:

                    foreach ($prepared[$name] as $related) {
                        $relation = $this->model()->$name();

                        $keyName = $relation->getRelated()->getKeyName();

                        $instance = $relation->findOrNew(array_get($related, $keyName));

                        if ($related[static::REMOVE_FLAG_NAME] == 1) {
                            $instance->delete();

                            continue;
                        }

                        array_forget($related, static::REMOVE_FLAG_NAME);

                        $instance->fill($related);

                        $instance->save();
                    }

                    break;
            }
        }
    }

    /**
     * Prepare input data for update.
     *
     * @param array $updates
     * @param bool  $oneToOneRelation If column is one-to-one relation.
     *
     * @return array
     */
    protected function prepareUpdate(array $updates, $oneToOneRelation = false)
    {
        $prepared = [];

        foreach ($this->builder->fields() as $field) {
            $columns = $field->column();

            // If column not in input array data, then continue.
            if (!array_has($updates, $columns)) {
                continue;
            }

            if ($this->invalidColumn($columns, $oneToOneRelation)) {
                continue;
            }

            $value = $this->getDataByColumn($updates, $columns);

            $value = $field->prepare($value);

            if (is_array($columns)) {
                foreach ($columns as $name => $column) {
                    array_set($prepared, $column, $value[$name]);
                }
            } elseif (is_string($columns)) {
                array_set($prepared, $columns, $value);
            }
        }

        return $prepared;
    }

    /**
     * @param string|array $columns
     * @param bool         $oneToOneRelation
     *
     * @return bool
     */
    protected function invalidColumn($columns, $oneToOneRelation = false)
    {
        foreach ((array) $columns as $column) {
            if ((!$oneToOneRelation && Str::contains($column, '.')) ||
                ($oneToOneRelation && !Str::contains($column, '.'))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Prepare input data for insert.
     *
     * @param $inserts
     *
     * @return array
     */
    protected function prepareInsert($inserts)
    {
        if ($this->isHasOneRelation($inserts)) {
            $inserts = array_dot($inserts);
        }

        foreach ($inserts as $column => $value) {
            if (is_null($field = $this->getFieldByColumn($column))) {
                unset($inserts[$column]);
                continue;
            }

            $inserts[$column] = $field->prepare($value);
        }

        $prepared = [];

        foreach ($inserts as $key => $value) {
            array_set($prepared, $key, $value);
        }

        return $prepared;
    }

    /**
     * Is input data is has-one relation.
     *
     * @param array $inserts
     *
     * @return bool
     */
    protected function isHasOneRelation($inserts)
    {
        $first = current($inserts);

        if (!is_array($first)) {
            return false;
        }

        if (is_array(current($first))) {
            return false;
        }

        return Arr::isAssoc($first);
    }

    /**
     * Set submitted callback.
     *
     * @param Closure $callback
     *
     * @return void
     */
    public function submitted(Closure $callback)
    {
        $this->submitted = $callback;
    }

    /**
     * Set saving callback.
     *
     * @param Closure $callback
     *
     * @return void
     */
    public function saving(Closure $callback)
    {
        $this->saving = $callback;
    }

    /**
     * Set saved callback.
     *
     * @param callable $callback
     *
     * @return void
     */
    public function saved(Closure $callback)
    {
        $this->saved = $callback;
    }

    /**
     * Ignore fields to save.
     *
     * @param string|array $fields
     *
     * @return $this
     */
    public function ignore($fields)
    {
        $this->ignored = array_merge($this->ignored, (array) $fields);

        return $this;
    }

    /**
     * @param array        $data
     * @param string|array $columns
     *
     * @return array|mixed
     */
    protected function getDataByColumn($data, $columns)
    {
        if (is_string($columns)) {
            return array_get($data, $columns);
        }

        if (is_array($columns)) {
            $value = [];
            foreach ($columns as $name => $column) {
                if (!array_has($data, $column)) {
                    continue;
                }
                $value[$name] = array_get($data, $column);
            }

            return $value;
        }
    }

    /**
     * Find field object by column.
     *
     * @param $column
     *
     * @return mixed
     */
    protected function getFieldByColumn($column)
    {
        return $this->builder->fields()->first(
            function (Field $field) use ($column) {
                if (is_array($field->column())) {
                    return in_array($column, $field->column());
                }

                return $field->column() == $column;
            }
        );
    }

    /**
     * Set original data for each field.
     *
     * @return void
     */
    protected function setFieldOriginalValue()
    {
//        static::doNotSnakeAttributes($this->model);

        $values = $this->model->toArray();

        $this->builder->fields()->each(function (Field $field) use ($values) {
            $field->setOriginal($values);
        });
    }

    /**
     * Set all fields value in form.
     *
     * @param $id
     *
     * @return void
     */
    protected function setFieldValue($id)
    {
        $relations = $this->getRelations();

        $this->model = $this->model->with($relations)->findOrFail($id);

//        static::doNotSnakeAttributes($this->model);

        $data = $this->model->toArray();

        $this->builder->fields()->each(function (Field $field) use ($data) {
            if (!in_array($field->column(), $this->ignored)) {
                $field->fill($data);
            }
        });
    }

    /**
     * Don't snake case attributes.
     *
     * @param Model $model
     *
     * @return void
     */
    protected static function doNotSnakeAttributes(Model $model)
    {
        $class = get_class($model);

        $class::$snakeAttributes = false;
    }

    /**
     * Get validation messages.
     *
     * @param array $input
     *
     * @return MessageBag|bool
     */
    protected function validationMessages($input)
    {
        $failedValidators = [];

        foreach ($this->builder->fields() as $field) {
            if (!$validator = $field->getValidator($input)) {
                continue;
            }

            if (($validator instanceof Validator) && !$validator->passes()) {
                $failedValidators[] = $validator;
            }
        }

        $message = $this->mergeValidationMessages($failedValidators);

        return $message->any() ? $message->first() : false;
    }

    /**
     * Merge validation messages from input validators.
     *
     * @param \Illuminate\Validation\Validator[] $validators
     *
     * @return MessageBag
     */
    protected function mergeValidationMessages($validators)
    {
        $messageBag = new MessageBag();

        foreach ($validators as $validator) {
            $messageBag = $messageBag->merge($validator->messages());
        }

        return $messageBag;
    }

    /**
     * Get all relations of model from callable.
     *
     * @return array
     */
    public function getRelations()
    {
        $relations = $columns = [];

        foreach ($this->builder->fields() as $field) {
            $columns[] = $field->column();
        }

        foreach (array_flatten($columns) as $column) {
            if (str_contains($column, '.')) {
                list($relation) = explode('.', $column);

                if (method_exists($this->model, $relation) &&
                    $this->model->$relation() instanceof Relation
                ) {
                    $relations[] = $relation;
                }
            } elseif (method_exists($this->model, $column) &&
                !method_exists(Model::class, $column)
            ) {
                $relations[] = $column;
            }
        }

        return array_unique($relations);
    }

    /**
     * Set action for form.
     *
     * @param string $action
     *
     * @return $this
     */
    public function setAction($action)
    {
        $this->builder()->setAction($action);

        return $this;
    }

    /**
     * Set field and label width in current form.
     *
     * @param int $fieldWidth
     * @param int $labelWidth
     *
     * @return $this
     */
    public function setWidth($fieldWidth = 8, $labelWidth = 2)
    {
        $this->builder()->fields()->each(function ($field) use ($fieldWidth, $labelWidth) {
            /* @var Field $field  */
            $field->setWidth($fieldWidth, $labelWidth);
        });

        $this->builder()->setWidth($fieldWidth, $labelWidth);

        return $this;
    }

    /**
     * Set view for form.
     *
     * @param string $view
     *
     * @return $this
     */
    public function setView($view)
    {
        $this->builder()->setView($view);

        return $this;
    }

    /**
     * Set title for form.
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title = '')
    {
        $this->builder()->setTitle($title);

        return $this;
    }


    /**
     * Disable form submit.
     *
     * @return $this
     */
    public function disableSubmit()
    {
        $this->builder()->options(['enableSubmit' => false]);

        return $this;
    }

    /**
     * Disable form reset.
     *
     * @return $this
     */
    public function disableReset()
    {
        $this->builder()->options(['enableReset' => false]);

        return $this;
    }

    /**
     * Get current resource route url.
     *
     * @param int $slice
     *
     * @return string
     */
    public function resource($slice = -2)
    {
        $segments = explode('/', trim(app('request')->getUri(), '/'));

        if ($slice != 0) {
            $segments = array_slice($segments, 0, $slice);
        }
        // # fix #1768
        if ($segments[0] == 'http:' && config('admin.secure') == true) {
            $segments[0] = 'https:';
        }
        return implode('/', $segments);
    }

    /**
     * Render the form contents.
     *
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
//        try {
            return $this->builder->render();
//        } catch (\Exception $e) {
//            return Handler::renderException($e);
//        }
    }

    /**
     * Get or set input data.
     *
     * @param string $key
     * @param null   $value
     *
     * @return array|mixed
     */
    public function input($key, $value = null)
    {
        if (is_null($value)) {
            return array_get($this->inputs, $key);
        }

        return array_set($this->inputs, $key, $value);
    }

    /**
     * Register builtin fields.
     *
     * @return void
     */
    public static function registerBuiltinFields()
    {
        $map = [
            'button'         => Button::class,
            'checkbox'       => Checkbox::class,
            'color'          => Color::class,
            'currency'       => Currency::class,
            'date'           => Date::class,
            'dateRange'      => DateRange::class,
            'datetime'       => Datetime::class,
            'dtime'          => DTime::class,
            'dateTimeRange'  => DatetimeRange::class,
            'datetimeRange'  => DatetimeRange::class,
            'decimal'        => Decimal::class,
            'display'        => Display::class,
            'divider'        => Divide::class,
            'divide'         => Divide::class,
            'embeds'         => Embeds::class,
            'editor'         => Editor::class,
            'email'          => Email::class,
            'file'           => File::class,
            'hasMany'        => HasMany::class,
            'hidden'         => Hidden::class,
            'id'             => Id::class,
            'image'          => Image::class,
            'ip'             => Ip::class,
            'map'            => Map::class,
            'mobile'         => Mobile::class,
            'month'          => Month::class,
            'multipleSelect' => MultipleSelect::class,
            'number'         => Number::class,
            'password'       => Password::class,
            'radio'          => Radio::class,
            'rate'           => Rate::class,
            'select'         => Select::class,
            'slider'         => Slider::class,
            'switch'         => SwitchField::class,
            'text'           => Text::class,
            'textarea'       => Textarea::class,
            'time'           => Time::class,
            'timeRange'      => TimeRange::class,
            'url'            => Url::class,
            'year'           => Year::class,
            'html'           => Html::class,
            'tags'           => Tags::class,
            'icon'           => Icon::class,
            'multipleFile'   => MultipleFile::class,
            'multipleImage'  => MultipleImage::class,
            'captcha'        => Captcha::class,
            'listbox'        => Listbox::class,
            'custom'        =>  Custom::class,
        ];

        foreach ($map as $abstract => $class) {
            static::extend($abstract, $class);
        }
    }

    /**
     * Register custom field.
     *
     * @param string $abstract
     * @param string $class
     *
     * @return void
     */
    public static function extend($abstract, $class)
    {
        static::$availableFields[$abstract] = $class;
    }

    /**
     * Remove registered field.
     *
     * @param array|string $abstract
     */
    public static function forget($abstract)
    {
        array_forget(static::$availableFields, $abstract);
    }

    /**
     * Find field class.
     *
     * @param string $method
     *
     * @return bool|mixed
     */
    public static function findFieldClass($method)
    {
        $class = array_get(static::$availableFields, $method);

        if (class_exists($class)) {
            return $class;
        }

        return false;
    }

    /**
     * Collect assets required by registered field.
     *
     * @return array
     */
    public static function collectFieldAssets()
    {
        if (!empty(static::$collectedAssets)) {
            return static::$collectedAssets;
        }

        $css = collect();
        $js = collect();

        foreach (static::$availableFields as $field) {
            if (!method_exists($field, 'getAssets')) {
                continue;
            }

            $assets = call_user_func([$field, 'getAssets']);

            $css->push(array_get($assets, 'css'));
            $js->push(array_get($assets, 'js'));
        }

        return static::$collectedAssets = [
            'css' => $css->flatten()->unique()->filter()->toArray(),
            'js'  => $js->flatten()->unique()->filter()->toArray(),
        ];
    }

    /**
     * Getter.
     *
     * @param string $name
     *
     * @return array|mixed
     */
    public function __get($name)
    {
        return $this->input($name);
    }

    /**
     * Setter.
     *
     * @param string $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->input($name, $value);
    }

    /**
     * Generate a Field object and add to form builder if Field exists.
     *
     * @param string $method
     * @param array  $arguments
     *
     * @return Field|void
     */
    public function __call($method, $arguments)
    {
        if ($className = static::findFieldClass($method)) {
            $column = array_get($arguments, 0, ''); //[0];

            $element = new $className($column, array_slice($arguments, 1));

            $this->pushField($element);

            return $element;
        }
    }

    /**
     * Render the contents of the form when casting to string.
     *
     * @return string
     * @throws \Throwable
     */
    public function __toString()
    {
        return $this->render();
    }
}
