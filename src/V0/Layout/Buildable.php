<?php

namespace Dls\Entity\V0\Layout;

interface Buildable
{
    public function build();
}
