<?php

namespace Dls\Entity\V0\Layout;

use Closure;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\MessageBag;

class Content implements Renderable
{
    /**
     * Content header.
     *
     * @var array
     */
    protected $header = [];

    /**
     * Content description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * @var Row[]
     */
    protected $rows = [];

    /**
     * 搜索器视图文件
     *
     * @var array
     */
    protected $searcher = [];

    /**
     * Content constructor.
     *
     * @param Closure|null $callback
     */
    public function __construct(\Closure $callback = null)
    {
        if ($callback instanceof Closure) {
            $callback($this);
        }
    }

    /**
     * Set header of content.
     *
     * @param string $header
     *
     * @return $this
     */
    public function header($icon = 'glyphicon glyphicon-list-alt', $title = '详情列表')
    {
        $this->header = ['icon' => $icon, 'title' => $title];

        return $this;
    }

    /**
     * Set description of content.
     *
     * @param string $description
     *
     * @return $this
     */
    public function description($description = '')
    {
        $this->description = $description;

        return $this;
    }

    /**
     * 传入视图文件 view('xx')
     *
     * @param $searcher
     * @return $this
     */
    public function searcher($view)
    {
        if (is_array($view)) {
            $this->searcher = $view;
            return $this;
        }

        array_push($this->searcher, $view);
        return $this;
    }


    /**
     * Alias of method row.
     *
     * @param mixed $content
     *
     * @return Content
     */
    public function body($content)
    {
        return $this->row($content);
    }

    /**
     * Add one row for content body.
     *
     * @param $content
     *
     * @return $this
     */
    public function row($content)
    {
        if ($content instanceof Closure) {
            $row = new Row();
            call_user_func($content, $row);
            $this->addRow($row);
        } else {
            $this->addRow(new Row($content));
        }

        return $this;
    }

    /**
     * Add Row.
     *
     * @param Row $row
     */
    protected function addRow(Row $row)
    {
        $this->rows[] = $row;
    }

    /**
     * Build html of content.
     *
     * @return string
     */
    public function build()
    {
        ob_start();

        foreach ($this->rows as $row) {
            $row->build();
        }

        $contents = ob_get_contents();

        ob_end_clean();

        return $contents;
    }

    /**
     * Set error message for content.
     *
     * @param string $title
     * @param string $message
     *
     * @return $this
     */
    public function withError($title = '', $message = '')
    {
        $error = new MessageBag(compact('title', 'message'));

        session()->flash('error', $error);

        return $this;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        $items = [
            'header'      => $this->header,
            'description' => $this->description,
            'content'     => $this->build(),
            'searcher'    => $this->searcher,
        ];

        return view('entity::layout.content', $items)->render();
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function __toString()
    {
        return $this->render();
    }
}
