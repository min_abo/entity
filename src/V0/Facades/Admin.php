<?php

namespace Dls\Entity\V0\Facades;

use Dls\Entity\V0\Form;
use Dls\Entity\V0\Grid\Table;
use Dls\Entity\V0\Layout\Content;
use Illuminate\Support\Facades\Facade;


/**
 * Class Admin
 *
 * @method static Form form($model, \Closure $callable)
 * @method static Content content(\Closure $callable = null)
 * @method static Table table(\Closure $callable)
 *
 */
class Admin extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Dls\Entity\V0\Admin::class;
    }
}
