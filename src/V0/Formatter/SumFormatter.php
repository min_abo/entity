<?php

namespace Dls\Entity\V0\Formatter;

use Dls\Entity\V0\FormatterAbstract;
use Dls\Entity\V0\Grid\Column;

class SumFormatter extends FormatterAbstract
{
    public function formatter(Column $field): string
    {
        return "(SUM(`{$field->getName()}`)) AS `{$field->getName()}`";
    }
}