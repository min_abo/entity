<?php

namespace Dls\Entity\V0\Formatter;

use Dls\Entity\V0\FormatterAbstract;
use Dls\Entity\V0\Grid\Column;

class CountFormatter extends FormatterAbstract
{
    public function formatter(Column $field): string
    {
        return "(COUNT(`{$field->getName()}`)) AS `{$field->getName()}`";
    }
}