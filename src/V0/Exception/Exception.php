<?php
/**
 * Created by PhpStorm.
 * User: huid
 * Date: 2018/9/26
 * Time: 22:06
 */

namespace Dls\Entity\V0\Exceptions;

use Throwable;

/**
 * Class Exception
 * @package Dls\Displayable\Exceptions
 */
class Exception extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}