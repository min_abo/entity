<?php

namespace Dls\Entity\V0\Grid;

use Illuminate\Support\Collection;

class Row
{
    public $columns;
    private $variables = [];

    public function __construct()
    {
        $this->columns = new Collection();
    }

    /**
     * Get the view variables of this field.
     *
     * @return array
     */
    protected function variables()
    {
        return array_merge($this->variables, [
            'columns' => $this->columns,
        ]);
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        return view('entity::grid.tr', $this->variables());
    }

}
