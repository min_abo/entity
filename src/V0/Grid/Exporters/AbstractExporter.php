<?php

namespace Dls\Entity\V0\Grid\Exporters;

use Dls\Entity\V0\Grid;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class AbstractExporter implements ExporterInterface
{
    /**
     * @var Grid
     */
    protected $grid;
    /**
     * Create a new exporter instance.
     *
     * @param $grid
     */
    public function __construct(Grid $grid = null)
    {
        if ($grid) {
            $this->setGrid($grid);
        }
    }
    /**
     * Set grid for exporter.
     *
     * @param Grid $grid
     *
     * @return $this
     */
    public function setGrid(Grid $grid)
    {
        $this->grid = $grid;
        return $this;
    }
    /**
     * Get table of grid.
     *
     * @return string
     */
    public function getTable()
    {
        return $this->grid->model()->eloquent()->getTable();
    }
    /**
     * Get data with export query.
     *
     * @param bool $toArray
     *
     * @return \Illuminate\Support\Collection|LengthAwarePaginator
     */
    public function getData()
    {
        return $this->grid->model()->usePaginate(false)->get();
    }
    /**
     * @param callable $callback
     * @param int      $count
     *
     * @return bool
     */
    public function chunk(callable $callback, $count = 100)
    {
        $grid = $this->grid->build();
        return $grid->model()->chunk($callback, $count);
    }

    /**
     * {@inheritdoc}
     */
    abstract public function export();
}