<?php

namespace Dls\Entity\V0\Grid\Outputs;

use Dls\Entity\V0\Grid;

abstract class AbstractOutput implements OutputInterface
{
    /**
     * @var Grid
     */
    protected $grid;
    /**
     * Create a new exporter instance.
     *
     * @param $grid
     */
    public function __construct(Grid $grid = null)
    {
        if ($grid) {
            $this->setGrid($grid);
        }
    }
    /**
     * Set grid for exporter.
     *
     * @param Grid $grid
     *
     * @return $this
     */
    public function setGrid(Grid $grid)
    {
        $this->grid = $grid;
        return $this;
    }
    /**
     * Get table of grid.
     *
     * @return string
     */
    public function getTable()
    {
        return $this->grid->model()->eloquent()->getTable();
    }

    /**
     * {@inheritdoc}
     */
    abstract public function data();
}