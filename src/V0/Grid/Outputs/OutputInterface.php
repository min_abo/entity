<?php

namespace Dls\Entity\V0\Grid\Outputs;

interface OutputInterface
{
    /**
     * Export data from grid.
     *
     * @return mixed
     */
    public function data();
}