<?php

namespace Dls\Entity\V0\Grid\Outputs;

class AjaxOutput extends AbstractOutput
{
    /**
     * {@inheritdoc}
     */
    public function data()
    {
        $callback = request()->get('jsoncallback') ?: 'callback';

        $grid = $this->grid->build();
        $paginator = $grid->model()->get();

        $data = [
            'data' => $grid->model()->buildData($paginator),
            'nums' => $paginator->total(),
            'pages' => $paginator->lastPage(),
        ];

        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        echo $response->setData($data)->setCallback($callback)->send();
        exit;
    }


}