<?php

namespace Dls\Entity\V0\Grid;

use Illuminate\Contracts\Support\Renderable;

/**
 * 通用性的替换对象，针对data-replace
 *
 * Class Replace
 * @package Dls\Entity\V0\Grid
 */
abstract class Replace implements Renderable
{
    protected $data = [];

    abstract public function init();

    /**
     * 设置replace的key跟value
     * 1,ysdk
     *
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function set(string $key, string $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * 一次性添加多个
     *
     * @param array $arr
     */
    public function multi(array $arr)
    {
        $this->data = array_merge($this->data, $arr);
        return $this;
    }

    /**
     * 拼凑data-replace内容
     *
     * @return string
     */
    protected function build()
    {
        $this->init();
        $str = '';
        foreach ($this->data as $key => $item) {
            $str .= "{$key},$item;";
        }
        return rtrim($str, ';');
    }

    public function render()
    {
        return $this->build();
    }

    public function __toString()
    {
        return $this->render();
    }
}