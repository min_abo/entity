<?php

namespace Dls\Entity\V0\Grid;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;

class Tr implements Renderable
{

    public $ths;
    private $variables = [];
    private $actions;

    public function __construct()
    {
        $this->ths = new Collection();
        $this->actions = new Collection();
    }

    /**
     * Get the view variables of this field.
     *
     * @return array
     */
    protected function variables()
    {
        return array_merge($this->variables, [
            'ths' => $this->ths,
            'actions' => $this->actions,
        ]);
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        return view('entity::grid.tr', $this->variables());
    }


    public function setActions($actions)
    {
        $this->actions->push($actions);
    }

}