<?php

namespace Dls\Entity\V0\Grid;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;

class Table implements Renderable
{
    // 列数据
    protected $columns;
    // 行数据
    protected $tr;
//    protected $class = ['table', 'table-striped', ' table-hover', 'table-func', 'text-center'];
    protected $class = ['table', 'table-striped', ' table-hover', 'text-center', 'table-bordered'];
    protected $pageNum = '20';
    protected $id = '';
    protected $noError;

    protected $url;
    private $variables = [];
    private $model;

    protected $actions;
    private $createBtn;

    public function __construct($model, \Closure $callback)
    {
        // 初始化表格行对象
        $this->tr = new Tr();
        // 初始化字段 查询
        $this->columns = new Collection();
        $this->actions = new Collection();
        $this->model = $model;
        $callback($this);
    }

    /**
     * Get the view variables of this field.
     *
     * @return array
     */
    protected function variables()
    {
        return array_merge($this->variables, [
            'url'     => $this->url,
            'pageNum' => $this->pageNum,
            'tr'      => $this->tr,
            'class'   => $this->getClass(),
            'id'      => $this->id,
            'createBtn' => $this->createBtn,
        ]);
    }

    /**
     * 追加th的字段
     *
     * @param string $field
     * @param string $text
     * @return Th
     */
    public function th(string $field, string $text)
    {
        $th = new Th($field, $text);
        $this->tr->ths->push($th);
        $this->columns->push($field);
        return $th;
    }

    public function editAction()
    {

        $str = <<<EOT
<a href='{$this->resource()}/<{id}>/edit' data-fn='layout-window-open'>
编辑
</a>
EOT;
        $this->tr->setActions($str);

    }

    public function deleteAction()
    {

        $str = <<<EOT
<a href='{$this->resource()}/<{id}>'>
删除
</a>
EOT;
        $this->tr->setActions($str);
    }

    public function createAction()
    {

        $str = <<<EOF
<div class="pull-right">
                    <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{$this->resource()}/create" class="btn btn-sm btn-primary" data-fn='layout-window-open'>
        <i class="fa fa-save"></i>&nbsp;&nbsp;新增
    </a>
</div>
                </div>
EOF;


        $this->createBtn = $str;
    }

    /**
     * Get current resource uri.
     *
     * @param string $path
     *
     * @return string
     */
    public function resource($path = null)
    {
        if (!empty($path)) {
            $this->resourcePath = $path;

            return $this;
        }

        if (!empty($this->resourcePath)) {
            return $this->resourcePath;
        }

        return app('request')->getPathInfo();
    }

    /**
     * @return Tr
     */
    public function getTr(): Tr
    {
        return $this->tr;
    }

    /**
     * @param Tr $tr
     */
    public function setTr(Tr $tr)
    {
        $this->tr = $tr;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return implode($this->class, ' ');
    }

    /**
     * @param array $class
     */
    public function setClass(array $class)
    {
        array_push($this->class, $class);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPageNum()
    {
        return $this->pageNum;
    }

    /**
     * @param mixed $pageNum
     */
    public function setPageNum($pageNum)
    {
        $this->pageNum = $pageNum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        array_push($this->class, 'table-func');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNoError()
    {
        return $this->noError;
    }

    /**
     * @param mixed $noError
     */
    public function setNoError($noError)
    {
        $this->noError = $noError;
        return $this;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        return view('entity::grid.table', $this->variables())->render();
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return Collection
     */
    public function getColumns(): Collection
    {
        return $this->columns;
    }

}