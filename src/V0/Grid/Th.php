<?php

namespace Dls\Entity\V0\Grid;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;

class Th implements Renderable
{

    // 字段名
    private $field;
    // 表头字段的展示文本
    private $text;
    // 是否可排序
    private $sortable = true;
    // 是否可见
    private $visible = true;
    // 是否可筛选
    private $filter = false;
    // 是否高亮
    private $highlight = true;
    // 是否冻结
    private $frozen = false;
    // 自定义样式
    private $class = '';
    // 是否求和
    private $sum = false;
    // 设置替换的内容
    private $replace = '';
    // 设置单元格颜色
    private $thColor = '';
    private $variables = [];

    public function __construct(string $field, string $text)
    {
        $this->field = $field;
        $this->text = $text;
    }


    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField(string $field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSortable()
    {
        return $this->sortable;
    }

    /**
     * @param mixed $sortable
     */
    public function setSortable($sortable)
    {
        $this->sortable = $sortable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param mixed $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     * @param mixed $highlight
     */
    public function setHighlight($highlight)
    {
        $this->highlight = $highlight;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrozen()
    {
        return $this->frozen;
    }

    /**
     * @param mixed $frozen
     */
    public function setFrozen($frozen)
    {
        $this->frozen = $frozen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param mixed $sum
     */
    public function setSum($sum)
    {
        $this->sum = $sum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReplace()
    {
        return $this->replace;
    }

    /**
     * @param mixed $replace
     */
    public function setReplace($replace)
    {
        $this->replace = $replace;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getThColor()
    {
        return $this->thColor;
    }

    /**
     * @param mixed $thColor
     */
    public function setThColor($thColor)
    {
        $this->thColor = $thColor;
        return $this;
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function render()
    {
        return view('entity::grid.th', $this->variables());
    }

    /**
     * Get the view variables of this field.
     *
     * @return array
     */
    protected function variables()
    {
        return array_merge($this->variables, [
            'field'     => $this->field,
            'text'      => $this->text,
            'frozen'    => $this->frozen,
            'filter'    => $this->filter,
            'class'     => $this->class,
            'highlight' => $this->highlight,
            'sum'       => $this->sum,
            'replace'   => $this->replace,
        ]);
    }

}