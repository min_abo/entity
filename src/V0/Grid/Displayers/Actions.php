<?php

namespace Dls\Entity\V0\Grid\Displayers;


class Actions extends AbstractDisplayer
{
    /**
     * @var array
     */
    protected $appends = [];

    /**
     * @var array
     */
    protected $prepends = [];

    /**
     * @var bool
     */
    protected $allowEdit = true;

    /**
     * @var bool
     */
    protected $allowDelete = true;

    /**
     * @var string
     */
    protected $resource;

    /**
     * @var
     */
    protected $key = 'id';

    /**
     * Append a action.
     *
     * @param $action
     *
     * @return $this
     */
    public function append($action)
    {
        array_push($this->appends, $action);

        return $this;
    }

    /**
     * Prepend a action.
     *
     * @param $action
     *
     * @return $this
     */
    public function prepend($action)
    {
        array_unshift($this->prepends, $action);

        return $this;
    }

    /**
     * Disable delete.
     *
     * @return void.
     */
    public function disableDelete()
    {
        $this->allowDelete = false;
    }

    /**
     * Disable edit.
     *
     * @return void.
     */
    public function disableEdit()
    {
        $this->allowEdit = false;
    }

    /**
     * Set resource of current resource.
     *
     * @param $resource
     *
     * @return void
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    /**
     * Get resource of current resource.
     *
     * @return string
     */
    public function getResource()
    {
        return $this->resource ?: parent::getResource();
    }

    /**
     * {@inheritdoc}
     */
    public function display($callback = null)
    {
        if ($callback instanceof \Closure) {
            $callback->call($this, $this);
        }

        $actions = [];
        if ($this->allowEdit) {
            array_push($actions, $this->editAction());
        }

        if ($this->allowDelete) {
            array_push($actions, $this->deleteAction());
        }
        return implode('', $actions);
    }

    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    public function getKey()
    {
        if ($this->key) {
            return $this->key;
        }

        return parent::getKey();
    }

    /**
     * Built edit action.
     *
     * @return string
     */
    protected function editAction()
    {
        $url = $this->getResource() . '/<{' . $this->getKey() . '}>/edit';

        return <<<EOT
<a href='$url' data-fn='layout-window-open' class='btn btn-xs btn-default'>
    <span><i class='fa fa-edit'></i> 编辑</span>
</a>
EOT;
    }

    /**
     * Built delete action.
     *
     * @return string
     */
    protected function deleteAction()
    {

        $url = $this->getResource() . '/<{' . $this->getKey() . '}>';
        $token = csrf_field();
        return <<<EOT
<label>    
    <form method='post' action='{$url}' onSubmit='if(!confirm("你是认真的吗？")){return false;}'>
        <input type='hidden' name='_method' value='DELETE'/>
        {$token}
        <button type="submit" class='btn btn-xs btn-danger'>
            <span><i class='ti-trash'></i> 删除</span>
        </button>
    </form>
</label>

EOT;
    }
}
