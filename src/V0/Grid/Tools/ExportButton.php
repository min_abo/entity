<?php

namespace Dls\Entity\V0\Grid\Tools;

use Dls\Entity\V0\Grid;

class ExportButton extends AbstractTool
{
    /**
     * Create a new CreateButton instance.
     *
     * @param Grid $grid
     */
    public function __construct(Grid $grid)
    {
        $this->grid = $grid;
    }

    /**
     * Render CreateButton.
     *
     * @return string
     */
    public function render()
    {
        if (!$this->grid->allowExport()) {
            return '';
        }

        return <<<EOT

<div class="btn-group pull-right">
    <a href="{$this->grid->getExportUrl()}" class="exportCsv btn btn-sm btn-primary" data-fn="self-exporter-csv">
        导出csv文件
    </a>
</div>

EOT;
    }

    public function title()
    {
        return '新建';
    }
}
