<?php

namespace Dls\Entity\V0\Grid\Tools;

use Dls\Entity\V0\Grid;

class CreateButton extends AbstractTool
{
    /**
     * Create a new CreateButton instance.
     *
     * @param Grid $grid
     */
    public function __construct(Grid $grid)
    {
        $this->grid = $grid;
    }

    /**
     * Render CreateButton.
     *
     * @return string
     */
    public function render()
    {
        if (!$this->grid->allowCreation()) {
            return '';
        }

        return <<<EOT

<div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{$this->grid->resource()}/create" class="btn btn-sm btn-success" data-fn='layout-window-open'>
        <i class="fa fa-save"></i>&nbsp;&nbsp;新增
    </a>
</div>

EOT;
    }

    public function title()
    {
        return '新建';
    }
}
