<?php

namespace Dls\Entity\V0;


use Dls\Entity\V0\Grid\Column;
use Dls\Entity\V0\Interfaces\Searchable;

abstract class Searcher implements Searchable
{

    /**
     * @var Column
     */
    protected $column;

    public function bind(Column $column)
    {
        $this->column = $column;
        return $this;
    }

}