<?php

namespace Dls\Entity\V0\Interfaces;

use Dls\Entity\V0\Grid\Column;
use Illuminate\Database\Eloquent\Builder;

interface Searchable
{
    public function search(Builder $builder);

    /**
     * @param Column $column
     * @return Searchable
     */
    public function bind(Column $column);
}