<?php

namespace Dls\Entity\V0\Form\Field;


use Dls\Entity\V0\Form\Field;

/**
 * 该组件的用法主要是用于适配form没办法查到数据的情况
 *
 * 返回的应该是一个collect的集合才会生效，具体参考OrderController的template方法的金额部分的写法
 *
 * Class Custom
 * @package Dls\Entity\V0\Form\Field
 */
class Custom extends Field
{
    private $custom;

    public function __construct($custom)
    {
        $this->custom = $custom;
    }

    /**
     * Render html field.
     *
     * @return string
     */
    public function render()
    {
        if ($this->custom instanceof \Closure) {
            $this->custom = $this->custom->call($this->form->model(), $this->form);
        }

        return $this->custom;
    }
}
