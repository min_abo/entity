<?php

namespace Dls\Entity\V0\Form\Field;

class TimeRange extends DateRange
{
    protected $format = 'HH:mm:ss';
}
