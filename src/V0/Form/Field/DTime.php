<?php

namespace Dls\Entity\V0\Form\Field;

use Dls\Entity\V0\Form\Field;

class DTime extends Field
{
    protected $format = 'YYYY-MM-DD HH:mm:ss';

    public function render()
    {
        //$this->defaultAttribute('style', 'width: 160px');

        return parent::render();
    }
}
