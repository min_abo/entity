<?php

namespace Dls\Entity\V0\Form\Field;

class MultipleSelect extends Select
{
    /** @var array data-key:xxx;data-value:xxx  */
    private $each = [];
    /** @var string $groupBy 分组字段 */
    private $groupBy = '';
    /**
     * @var array
     */
    protected $config = [];
    private $enum;


    public function url(string $url)
    {
        $this->url = $url;
        return $this;
    }

    public function each(string $key, string $value)
    {
        $this->each = [$key, $value];
        return $this;
    }

    public function groupBy(string $groupBy)
    {
        $this->groupBy = $groupBy;
        return $this;
    }

    public function enum(string $enum)
    {

        $this->enum = $enum;
        return $this;
    }


    /**
     * Set config for select2.
     *
     * all configurations see https://select2.org/configuration/options-api
     *
     * @param string $key
     * @param mixed  $val
     *
     * @return $this
     */
    public function config($key, $val)
    {
        $this->config[$key] = $val;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $var = [
            'enum' => $this->enum,
            'groupBy' => $this->groupBy,
            'dataKey' => $this->each[0] ?? '',
            'dataValue' => $this->each[1] ?? '',

        ];

        $this->addVariables($var);
        return parent::render();
    }
}
