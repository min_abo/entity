<?php

namespace Dls\Entity\V0\Form\Field;

use Dls\Entity\V0\Form\Field;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class File extends Field
{
    protected $filters = '';
    protected $maxSize = '200mb';
    protected $type = 'random';


    /**
     * 设置过滤的文件后缀
     * @return Field
     */
    public function filters(array $list)
    {
        $this->filters = implode(',', $list);
        return $this;
    }

    /**
     * 设置是上传文件的大小限制
     *
     * @param int $size
     * @param string $unit
     * @return Field
     */
    public function maxSize(int $size = 200, string $unit = 'mb')
    {
        $this->maxSize = $size . $unit;
        return $this;
    }

    /**
     * 此次文件上传的标识
     *
     * @param string $flag
     * @return Field
     */
    public function type(string $flag)
    {
         $this->type = $flag;
         return $this;
    }


    /**
     * Render file upload field.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        $this->addVariables($this->var());
        return parent::render();
    }

    protected function var()
    {
        if ($this->type == 'random') {
            throw new \InvalidArgumentException('此次文件上传的标识必须填写', 500);
        }

        return [
            'filters' => $this->filters,
            'maxSize' => $this->maxSize,
            'type' => $this->type,
        ];

    }
}
