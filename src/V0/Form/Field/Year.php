<?php

namespace Dls\Entity\V0\Form\Field;

class Year extends Date
{
    protected $format = 'YYYY';
}
