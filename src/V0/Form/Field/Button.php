<?php

namespace Dls\Entity\V0\Form\Field;


use Dls\Entity\V0\Form\Field;

class Button extends Field
{
    protected $class = 'btn-primary';
    /**
     * @var string
     */
    private $type;

    public function type(string $type)
    {
        $this->type = $type;
    }

    public function info()
    {
        $this->class = 'btn-info';

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $var = [
            'type' => $this->type,
            'value' => $this->value,
            'class' => $this->class,
        ];

        $this->setElementClass($this->class);
        $this->addVariables($var);
        return parent::render();
    }
}
