<?php

namespace Dls\Entity\V0\Form\Field;

use Dls\Entity\V0\Form\Field;

class Divide extends Field
{
    public function render()
    {
        return '<hr>';
    }
}
