<?php

namespace Dls\Entity\V0\Form\Field;

class Date extends Text
{

    protected $format = 'YYYY-MM-DD';

    public function format($format)
    {
        $this->format = $format;

        return $this;
    }

    public function prepare($value)
    {
        if ($value === '') {
            $value = null;
        }

        return $value;
    }

    public function render()
    {

        $this->defaultAttribute('style', 'width: 120px')
            ->defaultAttribute('data-fn', 'form-datepicker')
            ->defaultAttribute('data-type', 'sm');

        return parent::render();
    }
}
