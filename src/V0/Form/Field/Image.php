<?php

namespace Dls\Entity\V0\Form\Field;


class Image extends File
{

    /**
     * {@inheritdoc}
     */
    protected $view = 'entity::form.file';
    /**
     * 默认是单选
     *
     * @var bool
     */
    protected $multi = false;

    public function multi()
    {
        $this->multi = true;
        return $this;
    }

    /**
     * Render file upload field.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        return parent::render();
    }

    protected function var()
    {
        $var = parent::var();
        $var['image'] = true;
        $var['multi'] = $this->multi;
        return $var;
    }


}
