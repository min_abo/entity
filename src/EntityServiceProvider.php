<?php

namespace Dls\Entity;

use Illuminate\Support\ServiceProvider;

class EntityServiceProvider extends ServiceProvider
{

    protected $commands = [
        'Dls\Entity\Console\MakeCommand',
        'Dls\Entity\Console\InstallCommand',
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/V0/Resources/views', 'entity');
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__ . '/V0/Config/entity.php' => config_path('entity.php')]);
            $this->publishes([
                __DIR__ . '/V0/Resources/js/' => base_path('public/static/js/'),
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
    }
}
