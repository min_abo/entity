<?php

if (!function_exists('r')) {
    /**
     * ['id', 'game_id]
     *
     * @param string $url 传入的路由地址
     * @param array $parameters 参数
     * @return string
     */
    function r($url, $parameters = [], $func = [])
    {
        $path = route($url);

        if (empty($parameters)) {
            return $path;
        }
        $tmp = [];
        foreach ($parameters as $key => $value) {
            $tmp[$value] = "<{{$value}}>";
        }

        if (!empty($func)) { // ['game_id' => request('game_id', 0)]
            array_push($tmp, $func);
        }

        if (empty($tmp)) {
            return $path;
        }
        return  $path . '?' . urldecode(http_build_query($tmp));
    }
    
}
