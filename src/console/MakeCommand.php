<?php

namespace Dls\Entity\Console;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'entity:make';

    /**
     * php artisan admin:make AssetsController --model App\Models\Assets
     *
     * @var string
     */
    protected $description = 'Make empty entity controller';
    protected $directory = '';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->modelExists()) {
            $this->error('Model does not exists !');

            return false;
        }

        parent::handle();

    }


    /**
     * Replace the class name for the given stub.
     *
     * @param string $stub
     * @param string $name
     *
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        $baseClassName = class_basename($this->option('model'));

        $lcClassName = lcfirst($baseClassName);
        $formCallback = "top-{$lcClassName}-list";
        $tableId = "{$lcClassName}-list";

        return str_replace(
            [
                'DummyModelNamespace',
                'DummyModel',
                'formCallback',
                'tableId',
            ],
            [
                $this->option('model'),
                $baseClassName,
                $formCallback,
                $tableId
            ],
            $stub
        );
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        $directory = config('entity.controller');

        $namespace = ucfirst(basename($directory));
        return $rootNamespace."\Http\Controllers\\$namespace";
    }


    /**
     * Determine if the model is exists.
     *
     * @return bool
     */
    protected function modelExists()
    {
        $model = $this->option('model');
        if (empty($model)) {
            return true;
        }

        return class_exists($model);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if ($this->option('model')) {
            return __DIR__ . '/stubs/controller.stub';
        }

        return __DIR__ . '/stubs/blank.stub';
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the controller.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', null, InputOption::VALUE_REQUIRED,
                'The eloquent model that should be use as controller data source.', ],
        ];
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {

        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

}
