<?php

namespace Dls\Entity\Console;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    protected $name = 'entity:install';
    protected $description = 'install entity directory';
    protected $directory = '';

    public function handle()
    {
        $this->initEntityDirectory();
    }

    /** Initialize the admAin directory. */
    protected function initEntityDirectory()
    {
        $this->directory = config('entity.controller');

        if (is_dir($this->directory)) {
            $this->line("<error>{$this->directory} directory already exists !</error> ");

            return;
        }

        $this->makeDir('/');

        $this->line('<info>Entity directory was created:</info> '.str_replace(base_path(), '', $this->directory));

        $this->createExampleController();

    }


    protected function createExampleController()
    {
        $path = $this->directory . DIRECTORY_SEPARATOR . 'ExampleController.php';
        $contents = $this->getStub();

        $this->laravel['files']->put(
            $path,
            $contents
        );
        $this->line('<info>ExampleFormatter file was created:</info> '.str_replace(base_path(), '', $path));
    }


    /** Get the stub file for the generator. */
    protected function getStub()
    {
        return $this->laravel['files']->get(__DIR__ . '/stubs/blank.stub');
    }

    /** Make new directory. */
    protected function makeDir($path = '')
    {
        $this->laravel['files']->makeDirectory("{$this->directory}/$path", 0755, true, true);
    }


}
