# entity

#### 介绍
a entity manager

#### 软件架构
软


#### 安装教程
```js
composer require dls/entity
```

#### 使用说明
// 发布 模板/样式文件
```js
php artisan vendor:publish --provider="Dls\Entity\EntityServiceProvider"

php artisan dlsentity:install
```

// 生成实体Model
```js
php artisan dlsentity:model Model\\MwGame

php artisan dlsentity:controller MwGameController --model=App\\Model\\MwGame
```
