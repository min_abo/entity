# 公告
## v1.6 (2019-02-01)
### Added
- 新增多语言支持
- 新增jsonView，json 预览表单控件
- 新增自定义主题样式
- 支持设定`th`的属性

### changed
- 移除无用的Field，包含：
    - `Color, Current, DateRange, Datetime, DatetimeRange, Decimal, Embeds, Icon, Ip, ListBox, Mobile, Capatch, MultipleFile, MultipleImage`
    - `Number, Radio, Slider, Tags, TimeRange`

## v1.6.1 (2019-02-19)
### Added
- 新增生成model的命令，整合生成controller的命令
- 新增自定义操作列属性，即$column->attributes();
- 新增表单的help提示，即$form->text()->help();
- 新增支持获取地址栏的请求参数
- 新增关联查询，具体可查看dock.io的关联查询段

## v1.6.2 (2019-03-04)
### Added
- 新增timestamp的搜索器
- 支持关联条件查询

### Changed
- 更改所有的搜索器，将其支持关联关系的搜索

## v1.6.2 (2019-03-12)
### Changed
- 暂不预设表字段的填充

## v1.6.2 (2019-03-13)
### Fixed
- 修复关联查询器的失效问题